﻿namespace BlazorApp1.Client.Helper
{
    public class StringUtilities
    {
        public static string CustomToUpper(string value) => value.ToUpper();
    }
}
